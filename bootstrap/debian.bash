#!/bin/bash

# invoke this script like:
# curl -sS '<URL>' | bash -s [PROFILE]
# curl -sS 'https://gitlab.com/andanan/system-setup-config-store/-/raw/main/bootstrap/debian.bash' | bash -s [PROFILE]

if (return 0 &>/dev/null); then
	echo "Not intended to be sourced!" >&2
	exit 1
elif [[ ! -f /etc/debian_version ]]; then
	echo 'File not found: /etc/debian_version' >&2
	echo 'This script only works in Debian(ish) environments!' >&2
	exit 1
elif [[ ${DEBUG,,} != 'true' && $(id -u) != 0 ]]; then
	echo 'You must be root to run this bootstrapping script!' >&2
	exit 1
fi

# this is an non-interactive bootstrapping script for Debian* machines
# using the system-setup project as configuration management system.
# (* works on Debian derivates like Ubuntu and Mint as well)

warn() {
	if [[ $1 == '-f' ]]; then
		shift
		printf "$@" >&2
	else
		local IFS=' '
		printf "%s\n" "$*" >&2
	fi
}

die() {
	warn "${1:-Something went wrong!}"
	exit ${2:-1}
}

exec_cmd() {
	if [[ ${DEBUG,,} == 'true' ]]; then
		echo "$@"
	else
		"$@"
	fi
}

is_pkg_installed() {
	dpkg-query -W --showformat='${Status}\n' "$1" |& grep -q '^install ok installed$'
}

require_apt_pkgs() {
	local pkg= missing_pkgs=()
	for pkg in "$@"; do
		if ! is_pkg_installed "$pkg"; then
			missing_pkgs+=("$pkg")
		fi
	done
	if [[ ${#missing_pkgs[@]} -gt 0 ]]; then
		echo "Install packages:"
		printf '  - %s\n' "${missing_pkgs[@]}"
		exec_cmd apt-get update
		exec_cmd apt-get install --yes "${missing_pkgs[@]}"
	fi
}

git_install() {
	# Usage: git_install URL TARGET_DIR [BRANCH]
	local repo_url="$1"
	local target_dir="$2"
	local branch="$3"
	if [[ -d $target_dir/.git ]]; then
		(
			cd "$target_dir"
			local head_ref=$(git symbolic-ref -q HEAD)
			local remote_branch=$(git 'for-each-ref' --format='%(upstream:short)' "$head_ref")
			local remote="${remote_branch%%/*}"
			remote_branch="${remote_branch#*/}"
			local remote_url=$(git remote get-url "$remote")
			if [[ $remote_url == "$repo_url" ]]; then
				if [[ -z $branch || $remote_branch == "$branch" ]]; then
					exec_cmd git pull
				else
					warn 'Wrong remote branch configured!'
					warn "  expected: $branch"
					warn "  but was:  $remote_branch"
					return 1
				fi
			else
				warn 'Wrong remote configured!'
				warn "  expected: $repo_url"
				warn "  but was:  $remote_url"
		#		return 1
			fi
		)
	elif [[ -d $target_dir ]]; then
		die "Target dir exists but is not a git dir: $target_dir"
	else
		local git_args=("$repo_url" "$target_dir")
		if [[ -n "$branch" ]]; then
			git_args=(--single-branch --branch "$branch" "${git_args[@]}")
		fi
		exec_cmd git clone "${git_args[@]}"
	fi
}

git_make_install() {
	# Usage: git_make_install URL TARGET_DIR [BRANCH]
	git_install "$@"
	pushd "$2" &>/dev/null
	if [[ -f ./Makefile ]]; then
		exec_cmd make install
	fi
	popd &>/dev/null
}

prompt() {
	# Usage: prompt PROMPT [POSSIBLE_ANSWERS]
	local prompt="$1"; shift
	local answer='' _answer=''
	while [[ -z $answer ]]; do
		printf '\n%s' "$prompt" >&2
		if [[ $# -gt 0 ]]; then
			printf '\n' >&2
			local i= options=("$@")
			local max_idx=$((${#options[@]}-1))
			for i in "${!options[@]}"; do
				if [[ -z ${options[$i]} ]]; then
					continue # ignore empty options
				elif [[ ${options[$i]} == '--' ]]; then
					printf '%s\n' ' ---------------' >&2 ## create separator line
					continue
				fi
				printf "  (%${#max_idx}s) %s\n" $i "${options[$i]}" >&2
			done
			printf ' > ' >&2
		else
			printf ' ' >&2
		fi
		read -r _answer
		if [[ $# -gt 0 ]]; then
			local opt=
			for opt in "${!options[@]}"; do
				echo >$opt
				if [[ ${options[$opt]} == '--' ]]; then
					continue
				elif [[ $opt == "$_answer" || ${options[$opt]} == "$_answer" ]]; then
					answer="${options[$opt]}"
					break
				fi
			done
		else
			answer="$_answer"
			break # answer might be empty
		fi
	done
	printf '%s' "$answer"
}

setup_profile() {
	# Usage: setup_profile  PROFILE  CONFIG_REPO_DIR  TARGET_DIR
	local profile="$1"
	local cfg_dir="$2"
	local tgt_dir="$3"
	if [[ -z $tgt_dir ]]; then
		warn 'Target_dir required!'
		return 1
	fi
	local branch_dir="$cfg_dir/profiles/systems"
	if [[ -z $profile ]]; then
		local dirs=()
		for f in "$branch_dir"/*; do
			if [[ -d $f ]]; then
				dirs+=("$(basename "$f")")
			fi
		done
		profile=$(prompt "Choose a profile:" "${dirs[@]}" -- 'Cancel')
		if [[ $profile == 'Cancel' ]]; then
			exit 0
		fi
	fi
	local src_dir="$branch_dir/$profile"
	exec_cmd mkdir -p "$tgt_dir"
	# TODO: interpret the new conf module structure
	#	- system.cfg -> copy or link to: ${tgt_dir}/99_system.cfg
	#	- users.txt:
	#		- copy or link 'common/user_specs.cfg' to ${tgt_dir}/97_user_specs.cfg
	#		- create '${tgt_dir}/98_users.cfg'
	#	- modules.txt -> copy or link each module to: ${tgt_dir}/?+_${module_name}.cfg
	# TODO: create header comments like: 'ATTENTION: This File was genereated by ....'
	exec_cmd rm "$tgt_dir"/*
	exec_cmd ln -s "${src_dir}"/* "$tgt_dir"
}

#set -e # fail-fast

# ensure dependencies are installed
deps=(
	'git'
	'make'
)
require_apt_pkgs "${deps[@]}"

# TODO: as the above is the only section relying on APT, everything below could be moved to `bootstrap/common.bash`

# install system-setup config store via git
if [[ $0 == 'bash' && -z ${BASH_SOURCE[0]} ]]; then
	# piped to bash
	ss_cfg_dir="/opt/system-setup-cfg.git"
else
	script_file=$(readlink -f "${BASH_SOURCE[0]}")
	ss_cfg_dir="${script_file%/*}" # bootstrap dir
	ss_cfg_dir="${ss_cfg_dir%/*}" # main dir
	if [[ ! -d $ss_cfg_dir/profiles ]]; then
		warn "You seem to have a broken install of system-setup-config-store"
		die "  Directory not found: $ss_cfg_dir/profiles"
	fi
fi
git_make_install 'https://gitlab.com/andanan/system-setup-config-store.git' "$ss_cfg_dir"

# install system-setup via git
ss_dir="/opt/system-setup.git"
git_make_install 'https://gitlab.com/andanan/system-setup.git' "$ss_dir"

# setup PROFILE
profile="$1"
setup_profile "$profile" "$ss_cfg_dir" '/etc/system-setup.d'

# run system setup
exec_cmd "$ss_dir/system-setup.bash"

# TODO: setup user configs and run system-setup for users
#   - usernames defined in <system-profile-dir>/users.txt
#   - user configs in <ss_cfg_dir>/profiles/users/<username>/{modules.txt,user.cfg}

