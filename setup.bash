#!/bin/bash

# invoke this script like:
# curl -sS '<URL>' | bash -s [PROFILE]
# curl -sS 'https://gitlab.com/andanan/system-setup-config-store/-/raw/main/setup.bash' | bash -s [PROFILE]

if (return 0 &>/dev/null); then
	echo "Not intended to be sourced!" >&2
	exit 1
elif [[ ${DEBUG,,} != 'true' && $(id -u) != 0 ]]; then
	echo 'You must be root to run this bootstrapping script!' >&2
	exit 1
fi

# this is an interactive bootstrapping script using the system-setup
# project as configuration management system.

prompt() {
	# Usage: prompt PROMPT [POSSIBLE_ANSWERS]
	local prompt="$1"; shift
	local answer='' _answer=''
	while [[ -z $answer ]]; do
		printf '\n%s' "$prompt" >&2
		if [[ $# -gt 0 ]]; then
			printf '\n' >&2
			local i= options=("$@")
			local max_idx=$((${#options[@]}-1))
			for i in "${!options[@]}"; do
				if [[ -z ${options[$i]} ]]; then
					continue # ignore empty options
				elif [[ ${options[$i]} == '--' ]]; then
					printf '%s\n' ' ---------------' >&2 ## create separator line
					continue
				fi
				printf "  (%${#max_idx}s) %s\n" $i "${options[$i]}" >&2
			done
			printf ' > ' >&2
		else
			printf ' ' >&2
		fi
		read -r _answer
		if [[ $# -gt 0 ]]; then
			local opt=
			for opt in "${!options[@]}"; do
				echo >$opt
				if [[ ${options[$opt]} == '--' ]]; then
					continue
				elif [[ $opt == "$_answer" || ${options[$opt]} == "$_answer" ]]; then
					answer="${options[$opt]}"
					break
				fi
			done
		else
			answer="$_answer"
			break # answer might be empty
		fi
	done
	printf '%s' "$answer"
}

prompt_yesno() {
	# Usage: prompt_yesno PROMPT [DEFAULT]
	local prompt="$1"
	local default="$2"
	case ${default,,} in
		y|ye|yes)
			default='yes'
			prompt="$prompt [Y/n] "
		;;
		*)
			default='no'
			prompt="$prompt [N/y] "
		;;
	esac
	local answer=
	while [[ -z $answer ]]; do
		printf '%s' "$prompt" >&2
		read -r answer
		case ${answer,,} in
			'') answer="$default";;
			y|ye|yes) answer='yes';;
			n|no) answer='no';;
			*) answer='';;
		esac
	done
	printf '%s' "$answer"
}

prompt_or_default() {
	# Usage: prompt_or_default PROMPT DEFAULT_VALUE
	local prompt="$1"
	local default_value="$2"
	printf '%s (%s)' "$prompt" "$default_value" >&2
	local answer=
	read -r answer
	if [[ -z $answer ]]; then
		answer="$default_value"
	fi
	printf '%s' "$answer"
}

if [[ -z $os && -f /etc/debian_version ]]; then
	# debian(ish) environments
	os='debian'
fi

if [[ -z $os ]]; then
	echo 'System-Setup-Config-Store does not support your OS!' >&2
	echo 'Supported operating systems are:' >&2
	oses=(
		'Debian(ish) systems'
	)
	printf ' - %s\n' "${oses[@]}" >&2
	exit 1
fi

if command -v curl &>/dev/null; then
	cmd=(curl -sSL)
elif command -v wget &>/dev/null; then
	cmd=(wget -qO-)
else
	echo 'Neither curl nor wget are installed. Cannot download os-specific setup script file.' >&2
	exit 1
fi
url="https://gitlab.com/andanan/system-setup-config-store/-/raw/main/bootstrap/${os}.bash"
"${cmd[@]}" "$url" | bash -s "$@"

